#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <netdb.h>
#include <pthread.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netdb.h>
#include <limits.h>
#include <assert.h>
#include <stdatomic.h>
#include "pktgen.h"

enum rtt_pktgen_mode {
    mode_none = 0,
    mode_server,
    mode_client
};

static struct option longopts[] ={
    {"interface", required_argument, NULL, 'i'},
    {"delay", required_argument, NULL, 'd'},
    {"timeout", required_argument, NULL, 't'},
    {"repeat", required_argument, NULL, 'r'},
    {"addr", required_argument, NULL, 'a'},
    {"xdp_port", required_argument, NULL, 'x'},
    {"ipt_port", required_argument, NULL, 'p'},
    {"cont_port", required_argument, NULL, 'c'},
    {"out_file", required_argument, NULL, 'o'},
    {"loop", required_argument, NULL, 'l'},
    {"help" ,no_argument, NULL, 'h'},
    {0, 0, 0, 0}
};

static atomic_bool signal_initialized = ATOMIC_VAR_INIT(false);
static atomic_bool signal_handled = ATOMIC_VAR_INIT(false);

static void usage(void)
{
    fprintf(stderr, "usage:\n");
    fprintf(stderr, " client: mec_rtt_pktgen --client --timeout [sec] --cont_port [int] --out_file [file]\n");
    fprintf(stderr, " server: mec_rtt_pktgen --server --interface <nic_name> --addr <client_ipv4_addr> --delay [ms] --timeout [sec]\n \
                                --repeat [int] --xdp_port [int] --ipt_port [int] --cont_port [int]\n\n");
    fprintf(stderr, "options:\n");
    fprintf(stderr, "--server / --client: set running mode to server/client. server is sends packet, client is receive packet.\n");
    fprintf(stderr, "--interface: SERVER ONLY, set binding network interface. datagrms are pass to specified NIC.\n");
    fprintf(stderr, "--addr: SERVER ONLY, client IPv4 address.\n");
    fprintf(stderr, "--delay: SERVER ONLY, set delay per entire rounds, ms. Deafult: 500\n");
    fprintf(stderr, "--timeout: SERVER ONLY, set socket timeout, sec, Default: 5\n");
    fprintf(stderr, "--repeat: SERVER ONLY, set which number of packets will be sent for calculate mean. Deafult: 100\n");
    fprintf(stderr, "--loop: SERVER ONLY, how many time execute the rtt loop. Deafult: 1\n");
    fprintf(stderr, "--xdp_port: SERVER ONLY, port for XDP program.\n");
    fprintf(stderr, "--ipt_port: SERVER ONLY, port for iptables rule.\n");
    fprintf(stderr, "--cont_port: port for client container.\n");
    fprintf(stderr, "--out_file: CLIENT ONLY, specify full file path of output file. a file will be overwritten each received latency data.\n\n");
}

int pktgen_server_run(struct pktgen_socks *socks, char* host,
        char* xdp_port, char* ipt_port, char* cont_port, int delay, int timeout, int repeat)
{
    int64_t seq = 0;
    struct pktgen_msg msg = {0};
    struct pktgen_remaddrs remaddrs = {0};
    
    struct addrinfo *reminfo = NULL;
    struct timespec timestamp_send = {0};
    struct timespec timestamp_receive = {0};
    struct timespec ts_sleep = {0};
    struct timespec rtt_latency = {0};

    int ret;
    if((ret = getaddrinfo(host, xdp_port, NULL, &reminfo)) < 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        return EXIT_FAILURE;
    }
    memcpy(&remaddrs.xdp_remaddr, reminfo->ai_addr, sizeof(remaddrs.xdp_remaddr));
    freeaddrinfo(reminfo);

    if((ret = getaddrinfo(host, ipt_port, NULL, &reminfo)) < 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        return EXIT_FAILURE;
    }
    memcpy(&remaddrs.ipt_remaddr, reminfo->ai_addr, sizeof(remaddrs.ipt_remaddr));
    freeaddrinfo(reminfo);

    if((ret = getaddrinfo(host, cont_port, NULL, &reminfo)) < 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        return EXIT_FAILURE;
    }
    memcpy(&remaddrs.cont_remaddr, reminfo->ai_addr, sizeof(remaddrs.cont_remaddr));
    freeaddrinfo(reminfo);

    // Go go go!
    if((ret = get_avg_rtt(socks, &remaddrs, repeat, &seq)) < 0) {
        fprintf(stderr, "get_avg_rtt: error is occured.\n");
        return EXIT_FAILURE;
    }
    return 0;
}

int pktgen_client_run(struct pktgen_socks *socks, FILE *fp)
{
    int ret, fd;
    struct pktgen_msg msg = {0};
    struct sockaddr_in remaddr = {0};

    fd = fileno(fp);
    

    for (;;)
    {
        if((ret = receive_pkt_msg(socks->cont_sock, &msg, NULL, &remaddr, true)) < 0) {
            fprintf(stderr, "client.receive_pkt_msg: invalid packet.\n");
            return EXIT_FAILURE;
        }

        if(msg.msg_type == rtt_measure) {
            if((ret = send_pkt_cont(socks->cont_sock, &remaddr, &msg)) < 0) {
                fprintf(stderr, "client.send_pkt_cont: %s\n", gai_strerror(ret));
                return EXIT_FAILURE;
            }
        }

        else if(msg.msg_type == rtt_latency) {
            ftruncate(fd, 0);
            fprintf(fp, "xdp:%lld ipt:%lld cont:%lld", msg.latency_xdp, msg.latency_ipt, msg.latency_cont);
            rewind(fp);
        }

        else {
            fprintf(stderr, "pktgen_client_run.recvmsg: Invalid packet.");
            continue;
        } 
    }
}

int main(int argc, char **argv)
{
    //TODO: Add signal handler
    FILE *fp;
    struct pktgen_socks *socks = NULL;
    enum rtt_pktgen_mode mode = mode_none;
    char *interface = NULL;
    int delay = 500;
    char *client_addr = NULL;
    char *xdp_port = NULL;
    char *ipt_port = NULL;
    char *cont_port = NULL;
    char *file = NULL;
    int timeout = 5;
    int repeat = 10;
    int loop = 1;
    int c, res, nargc = argc;

    int i = 0;
    int ret = 0;

    // Disable stdout buffer
    setvbuf(stdout, NULL, _IONBF, 0); 

    if(argc >=2) {
        if(!strcmp(argv[1], "--server")) {
            mode = mode_server;
            nargc--;
        }
        else if(!strcmp(argv[1], "--client")) {
            mode = mode_client;
            nargc --;
        }
        else {
            usage();
            return EXIT_FAILURE;
        }
    }
    else {
        usage();
        return EXIT_FAILURE;
    }

    if(mode == mode_server)
    {
        while((c = getopt_long_only(nargc, argv + 1, "i:d:t:r:a:x:p:c:l:h", longopts, NULL)) != -1)
        {
            switch (c) {
                case 'i':
                    interface = strdup(optarg);
                    break;
                case 'd':
                    delay = atoi(optarg);
                    break;
                case 't':
                    timeout = atoi(optarg);
                    break;
                case 'r':
                    repeat = atoi(optarg);
                    break;
                case 'a':
                    client_addr = strdup(optarg);
                    break;
                case 'x':
                    xdp_port = strdup(optarg);
                    break;
                case 'p':
                    ipt_port = strdup(optarg);
                    break;
                case 'c':
                    cont_port = strdup(optarg);
                    break;
                case 'l':
                    loop = atoi(optarg);
                    break;
                case 'h':
                    usage();
                    return EXIT_FAILURE;
            }
        }
    }

    else if(mode == mode_client)
    {
        while((c = getopt_long_only(nargc, argv + 1, "t:c:o:h", longopts, NULL)) != -1)
        {
            switch(c) {
                case 't':
                    timeout = atoi(optarg);
                    break;
                case 'c':
                    cont_port = strdup(optarg);
                    break;
                case 'o':
                    file = strdup(optarg);
                    break;
                case 'h':
                    usage();
                    return EXIT_FAILURE;
            }
        }
    }


    if(mode == mode_client)
    {
        // Validate input options
        if(!file) {
            fprintf(stderr, "err: --out_file is not specified. --help to print usage.\n");
            return EXIT_FAILURE;
        }
        
        if(!cont_port) {
            fprintf(stderr, "err: --cont_port is not specified. --help to print usage.\n");
            return EXIT_FAILURE;
        }

        socks = pktgen_client_init(atoi(cont_port), file, timeout, &fp);
        if(!socks) {
            fprintf(stderr, "pktgen client init failed.\n");
            return EXIT_FAILURE;
        }
        // Disable file buffer
        setvbuf(fp, NULL, _IONBF, 0);

        ret = pktgen_client_run(socks, fp);
    }

    else if(mode == mode_server)
    {
        // Validate input options
        if (!interface) {
            fprintf(stderr, "err: --interface is not specified. --help to print usage.\n");
            return EXIT_FAILURE;
        }
        
        if(!client_addr) {
            fprintf(stderr, "err: --addr is not specified. --help to print usage.\n");
            return EXIT_FAILURE;
        }

        if (!xdp_port || !ipt_port || !cont_port) {
            fprintf(stderr, "err: ports are not specified. --help to print usage.\n");
            return EXIT_FAILURE;
        }

        if(interface && client_addr)
        {
            socks = pktgen_server_init(interface, atoi(xdp_port), atoi(ipt_port), atoi(cont_port), timeout);
            for(i = 0; i < loop ; i++) {
                ret = pktgen_server_run(socks, client_addr, xdp_port, ipt_port, cont_port, delay, timeout, repeat);
            }
        }
    }

    if(mode == mode_server) {
        close(socks->xdp_sock);
        close(socks->ipt_sock);
    }
    close(socks->cont_sock);    
    fcloseall();
    
    return ret;
}