#include <stdint.h>
#include <stdbool.h>

struct pktgen_socks {
    int xdp_sock;
    int ipt_sock;
    int cont_sock;
};

struct pktgen_remaddrs {
    struct sockaddr_in xdp_remaddr;
    struct sockaddr_in ipt_remaddr;
    struct sockaddr_in cont_remaddr;
};

struct pktgen_timeval {
    int64_t tv_sec;
    int64_t tv_nsec;
};

struct pktgen_msg {
    int64_t seq;
    int8_t msg_type;
    int64_t latency_xdp;
    int64_t latency_ipt;
    int64_t latency_cont;
};

enum pktgen_pkt_type {
    rtt_measure = 0,
    rtt_latency
};

// Common functions
ssize_t receive_pkt_msg(int sock_fd, struct pktgen_msg *msg, struct timespec *tstamp,
        struct sockaddr_in *remaddr, bool client);
int get_avg_rtt(struct pktgen_socks *socks, struct pktgen_remaddrs *remaddrs, int repeat, int64_t *seq);

struct pktgen_socks *pktgen_server_init(char *interface, int xdp_port, int ipt_port, int cont_port, int timeout);
struct pktgen_socks *pktgen_client_init(int cont_port, char *file, int timeout, FILE **fp);