#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/net_tstamp.h>
#include <linux/sockios.h>
#include <poll.h>
#include <linux/errqueue.h>
#include "pktgen.h"


int get_if_address(int fd, char *ifname, struct in_addr *addr)
{
    int ret;
    struct ifreq ifr;
    struct sockaddr_in *sin = (struct sockaddr_in *)&ifr.ifr_addr;

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    ifr.ifr_addr.sa_family = AF_INET;
    if ((ret = ioctl(fd, SIOCGIFADDR, &ifr)) < 0) {
        perror("SIOCGIFADDR");
        return ret;
    }
    *addr = sin->sin_addr;
    return 0;
}

int enable_hw_timestamp(int fd, char *ifname)
{
    int ret;
    struct hwtstamp_config config = {.flags = 0, .tx_type= HWTSTAMP_TX_ON};
    struct ifreq ifr;
    unsigned int opt;
    int enabled = 1;

    config.rx_filter = HWTSTAMP_FILTER_ALL;
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
    ifr.ifr_addr.sa_family = AF_INET;
    ifr.ifr_data = (void *)&config;

    if((ret = ioctl(fd, SIOCSHWTSTAMP, &ifr)) < 0)
    {
        perror("SIOCSHWTSTAMP");
        return ret;
    }

    opt = SOF_TIMESTAMPING_RX_HARDWARE |
        SOF_TIMESTAMPING_TX_HARDWARE |
        SOF_TIMESTAMPING_RAW_HARDWARE |
        SOF_TIMESTAMPING_OPT_CMSG;
    if((ret = setsockopt(fd, SOL_SOCKET, SO_TIMESTAMPING, (char *)&opt, sizeof(opt))) < 0)
    {
        perror("SO_TIMESTAMPING");
        return ret;
    }
    
    if ((ret = setsockopt(fd, SOL_SOCKET, SO_SELECT_ERR_QUEUE, &enabled,
                    sizeof(enabled))) < 0) {
        perror("SO_SELECT_ERR_QUEUE");
        return ret;
    }
    if((ret = setsockopt(fd, IPPROTO_IP, IP_PKTINFO, &enabled, sizeof(enabled))) < 0)
    {
        perror("IP_PKTINFO");
        return ret;
    }

    return 0;
}

inline ssize_t send_pkt_common(int fd_sock, struct sockaddr_in *remaddr, struct iovec *iov)
{
    struct msghdr m = {0};
    ssize_t size;
    int ret;

    m.msg_name = remaddr;
    m.msg_namelen = sizeof(struct sockaddr_in);
    m.msg_iov = iov;
    m.msg_iovlen = 1;

    errno = 0;
    if ((size = sendmsg(fd_sock, &m, 0)) <= 0) {
        if (errno == EAGAIN) {
            fprintf(stderr, "sendmsg: Request timed out.\n");
            return size;
        }
        perror("sendmsg");
        return size;
    }
    return size;
}

ssize_t send_pkt_cont(int cont_sock, struct sockaddr_in *remaddr, struct pktgen_msg *msg)
{
    struct iovec iov = {msg, sizeof(*msg)};

    return send_pkt_common(cont_sock, remaddr, &iov);
}


int bind_socket(int sock, char *interface, int port)
{
    int ret;
    struct sockaddr_in myaddr = {0};
    if(interface == NULL) { // client
        myaddr.sin_port = htons(port);
        myaddr.sin_family = AF_INET;
        myaddr.sin_addr.s_addr = INADDR_ANY;

        if((ret = bind(sock, (struct sockaddr *)&myaddr, sizeof(myaddr))) < 0) {
            perror("client_init:cont_bind");
            return ret;
        }
        return 0;    
    }

    if((ret = get_if_address(sock, interface, &myaddr.sin_addr)) < 0) {
        perror("server_init:get_if_address");
        return ret;
    
    }
    myaddr.sin_port = htons(port);
    myaddr.sin_family = AF_INET;
    // server bind
    if((ret = bind(sock, (struct sockaddr *)&myaddr, sizeof(myaddr))) < 0) {
        perror("server_init:xdp_bind");
        return ret;
    }

    return 0;
}

int parse_control_msg(struct msghdr *m, struct timespec *tstamp, int *stamp_found)
{
    struct cmsghdr *cm;
    struct timespec *received_stamp;
    struct sock_extended_err *exterr;

    *stamp_found = 0;
    for ( cm = CMSG_FIRSTHDR(m); cm; cm = CMSG_NXTHDR(m, cm))
    {
        switch (cm->cmsg_level) {
            case SOL_SOCKET:
                switch (cm->cmsg_type) {
                    case SO_TIMESTAMPING:
                        received_stamp = (struct timespec *)CMSG_DATA(cm);
                        *tstamp = received_stamp[2];
                        *stamp_found = 1;
                        break;
                    default:
                        fprintf(stderr, "Unexpected cmsg. level:SOL_SOCKET type:%d\n", cm->cmsg_type);
                        return -1;
                }
                break;
            case IPPROTO_IP:
                switch (cm->cmsg_type) {
                    case IP_RECVERR:
                        exterr = (struct sock_extended_err *)CMSG_DATA(cm);
                        if(!(exterr->ee_errno == ENOMSG &&
                                exterr->ee_origin == SO_EE_ORIGIN_TIMESTAMPING))
                            fprintf(stderr, "Unexpected recverr errno '%s', origin %d\n",
                                strerror(exterr->ee_errno), exterr->ee_origin);
                        break;
                    case IP_PKTINFO:
                        break;
                    default:
                        fprintf(stderr, "Unexpected cmsg level: IPPROTO_IP, type:%d\n",
                            cm->cmsg_type);
                        return -1;
                }
                break;
            default:
                fprintf(stderr, "Unexpected cmsg level:%d, type:%d\n",
                    cm->cmsg_level, cm->cmsg_type);
                return -1;
        }
    }
    return 0;
}

ssize_t receive_pkt_common(int sock_fd, struct iovec *iov, struct timespec *tstamp,
        struct sockaddr_in *remaddr, bool client)
{
    struct msghdr m = {0};
    char ctrlbuf[1024];
    ssize_t size;
    int ret;
    int stamp_found;

    m.msg_iov = iov;
    m.msg_iovlen = 1;
    m.msg_name = remaddr;
    m.msg_namelen = sizeof(struct sockaddr_in);
    m.msg_control = ctrlbuf; //TODO: check this
    m.msg_controllen = sizeof(ctrlbuf);

    errno = 0;
    if ((size = recvmsg(sock_fd, &m, 0)) < 0) {
        if (errno == EAGAIN) {
            fprintf(stderr, "recvmsg: Request timed out.\n");
            return size;
        }
        perror("recvmsg");
        return size;
    }
    if(client) return 0;
    if ((ret = parse_control_msg(&m, tstamp, &stamp_found)) < 0) return ret;
    
    return size;
}

ssize_t receive_pkt_msg(int sock_fd, struct pktgen_msg *msg, struct timespec *tstamp,
        struct sockaddr_in *remaddr, bool client)
{
    struct iovec iov = {msg, sizeof(*msg)};
    ssize_t size = receive_pkt_common(sock_fd, &iov, tstamp, remaddr, client);

    return size;
}

int get_tx_timestamp(int sock_fd, struct timespec *tstamp)
{
    struct pollfd fds[1];
    struct sockaddr_in remaddr;
    struct msghdr m = {0};
    struct pktgen_msg *msg;
    char pktbuf[2048];
    char ctrlbuf[1024];
    struct iovec iov = {pktbuf, sizeof(pktbuf)};
    int ret;
    int size, headsize;
    int stamp_found = 0;

    fds[0].fd = sock_fd;
    fds[0].events = POLLPRI;

    if((ret = poll(fds, 1, 5 * 1000)) == 0) {
        fprintf(stderr, "Timeout to receive tx timestamp\n");
        exit(EXIT_FAILURE);
        return -1;
    }
    else if (ret < 0) {
        perror("get_tx_timestamp.poll");
        return ret;
    }

    m.msg_iov = &iov;
    m.msg_iovlen = 1;
    m.msg_name = (void *)&remaddr;
    m.msg_namelen = sizeof(remaddr);
    m.msg_control = ctrlbuf;
    m.msg_controllen = sizeof(ctrlbuf);

    errno = 0;
    if((size = recvmsg(sock_fd, &m, MSG_ERRQUEUE)) < 0) {
        if(errno == EAGAIN) {
            fprintf(stderr, "tx_timestamp.recvmsg(errqueue): Request timed out.\n");
            return size;
        }
        perror("tx_timestamp.recvmsg(errqueue");
        return size;
    }

    if(size < sizeof(*msg)) {
        fprintf(stderr, "Invalid packet size on tx.\n");
        return -1;
    }
    
    if ((ret = parse_control_msg(&m, tstamp, &stamp_found)) < 0){
        fprintf(stderr, "tx_timestamp.parse_control_msg: Invalid timestamp msg.\n");
        return ret;
    }

    return 0;
}


struct pktgen_socks *pktgen_server_init(char *interface, int xdp_port, int ipt_port, int cont_port, int timeout)
{
    struct pktgen_socks *socks = (struct pktgen_socks *)calloc(1, sizeof(*socks));
    assert(socks);
    
    int ret;
    if((ret = pktgen_server_socket_init(interface, timeout, xdp_port, &(socks->xdp_sock))) < 0) {
        perror("xdp_sock.init");
        return NULL;
    }
    
    if((ret = pktgen_server_socket_init(interface, timeout, ipt_port, &(socks->ipt_sock))) < 0) {
        perror("ipt_sock.init");
        return NULL;
    }
    
    if((ret = pktgen_server_socket_init(interface, timeout, cont_port, &(socks->cont_sock))) < 0) {
        perror("cont_sock.init");
        return NULL;
    }

    if((ret = bind_socket(socks->xdp_sock, interface, xdp_port)) < 0) {
        perror("xdp_socket.bind");
        return NULL;
    }

    if((ret = bind_socket(socks->ipt_sock, interface, ipt_port)) < 0) {
        perror("ipt_socket.bind");
        return NULL;
    }

    if((ret = bind_socket(socks->cont_sock, interface, cont_port)) < 0) {
        perror("cont_socket.bind");
        return NULL;
    }
    return socks;    
}

int pktgen_server_socket_init(char *interface, int timeout, int port, int *sock)
{
    int ret;

    //socket
    if ((*sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket.dgram");
        return -1;
    }

    // bind to device
    if ((ret = setsockopt(*sock, SOL_SOCKET, SO_BINDTODEVICE, interface,
                    strlen(interface)+1)) < 0) {
        perror("setsockopt.SO_BINDTODEVICE");
        return -1;
    }

    // timeout
    struct timeval timeo = {timeout, 0};
    if((ret = setsockopt(*sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeo,
            sizeof(timeo))) < 0) {
        perror("setsockopt.SO_SNDTIMEO");
        return -1;
    }
    if((ret = setsockopt(*sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeo,
            sizeof(timeo))) < 0) {
        perror("setsockopt.SO_RCVTIMEO");
        return -1;
    }

    // enable hw timestamping
    if((ret = enable_hw_timestamp(*sock, interface)) < 0) {
        perror("enable_hw_timestamp_sock");
        return -1;
    }

    return 0;
}

struct pktgen_socks *pktgen_client_init(int cont_port, char *file, int timeout, FILE **fp)
{
    struct pktgen_socks *socks = (struct pktgen_socks *)calloc(1, sizeof(*socks));
    int ret;

    assert(socks);

    //socket
    if ((socks->cont_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket.dgram");
        return NULL;
    }

    // timeout
    struct timeval timeo = {timeout, 0};
    if((ret = setsockopt(socks->cont_sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeo,
            sizeof(timeo))) < 0) {
        perror("setsockopt.SO_SNDTIMEO");
        return NULL;
    }

    socks->xdp_sock = 0;
    socks->ipt_sock = 0;

    // bind
    if((ret = bind_socket(socks->cont_sock, NULL, cont_port)) < 0) {
        return NULL;
    }

    // log file init
    *fp = fopen(file, "w");
    if(!(*fp)) {
        perror("pktgen_client_init.fopen");
        return NULL;
    }
    return socks;
}


int get_avg_rtt(struct pktgen_socks *socks, struct pktgen_remaddrs *remaddrs, int repeat, int64_t *seq)
{
    int ret;
    struct pktgen_msg m = {0};
    struct timespec latency = {0};

    if((ret = __get_avg_rtt_loop(socks->xdp_sock, repeat, &(remaddrs->xdp_remaddr), &latency, seq)) < 0) {
        fprintf(stderr, "get_avg_rtt.xdp.__get_avg_rtt_loop: error is occured.\n");
        return -1;
    }
    m.latency_xdp = latency.tv_nsec;
    fprintf(stdout, "\n%lld: xdp:%lld ", *seq, latency.tv_nsec);
    
    if((ret = __get_avg_rtt_loop(socks->ipt_sock, repeat, &(remaddrs->ipt_remaddr), &latency, seq)) < 0) {
        fprintf(stderr, "get_avg_rtt.ipt.__get_avg_rtt_loop: error is occured.\n");
        return -1;
    }
    fprintf(stdout, "ipt:%lld ", latency.tv_nsec);
    m.latency_ipt = latency.tv_nsec;    
    
    if((ret = __get_avg_rtt_loop(socks->cont_sock, repeat, &(remaddrs->cont_remaddr), &latency, seq)) < 0) {
        fprintf(stderr, "get_avg_rtt.cont.__get_avg_rtt_loop: error is occured.\n");
        return -1;
    }
    fprintf(stdout, "cont:%lld", latency.tv_nsec);
    m.latency_cont = latency.tv_nsec;

    // build result message
    m.msg_type = rtt_latency;
    send_pkt_cont(socks->cont_sock, &(remaddrs->cont_remaddr), &m);
    get_tx_timestamp(socks->cont_sock, &latency); // consume ERRQUEUE
}

int __get_avg_rtt_loop(int fd_sock, int repeat, struct sockaddr_in *remaddr,
         struct timespec *latency, int64_t *seq)
{
    int ret, i;
    struct timespec ts_send = {0};
    struct timespec ts_recv = {0};
    struct timespec ts_sleep = {0};
    struct pktgen_msg m_send = {0};
    struct pktgen_msg m_recv = {0};
    int64_t rtt = 0;

    struct iovec iov = {&m_send, sizeof(m_send)};
    
    // set msg params
    m_send.msg_type = rtt_measure;

    for(i = 0; i < repeat; i++)
    {
        (*seq)++;
        m_send.seq = (*seq);

        if((ret = send_pkt_common(fd_sock, remaddr, &iov)) < 0) {
            fprintf(stderr, "send_pkt_common: %s\n", gai_strerror(ret));
            return -1;
        }
        // get tx_timestamp
        if((ret = get_tx_timestamp(fd_sock, &ts_send)))
        {
            fprintf(stderr, "get_tx_timestamp: no tx timestamp.\n");
            return -1;
        }
        while(1)
        {
            // get rx_timestamp
            if((ret = receive_pkt_msg(fd_sock, &m_recv, &ts_recv, remaddr, false)) < 0)
            {
                fprintf(stderr, "receive_pkt_msg: invalid packet.\n");
                return -1;
            }
            if(m_recv.seq == m_send.seq) // calculate rtt
            {
                rtt = rtt + (ts_recv.tv_nsec - ts_send.tv_nsec) + ((ts_recv.tv_sec - ts_send.tv_sec) * 1000000000);
                break;
            }
        }
        // delay per packet (500us)
        ts_sleep.tv_sec = 0;
        ts_sleep.tv_nsec = 50000000;
        do {
            ret = nanosleep(&ts_sleep, &ts_sleep);
        } while (ret && errno == EINTR);

    }
    latency->tv_nsec = rtt / repeat;

    return 0;
}