import sys
import os
import getopt
try:
    opts, args = getopt.getopt(sys.argv[1:], "b:i:a:t:d:x:p:c:r:h",
        ["binary=", "interface=", "addr_list=", "timeout=", "delay=", "xdp_port=",\
	 "ipt_port=", "cont_port=", "repeat=", "help"])
except getopt.GetoptError as err:
    print(str(err))
    sys.exit(1)

binary = None
interface = None
addr_list = None
timeout = "5"
delay = "100"
xdp_port = None
ipt_port = None
cont_port = None
repeat = "100"

for o, a in opts:
    if o in ("--b", "--binary"):
        binary = a + " "
    elif o in ("--i", "--interface"):
        interface = a
    elif o in ("--a", "--addr_list"):
        addr_list = a
    elif o in ("--t", "--timeout"):
        timeout = a
    elif o in ("--d", "--delay"):
        delay = a
    elif o in ("--x", "--xdp_port"):
        xdp_port = a
    elif o in ("--p", "--ipt_port"):
        ipt_port = a
    elif o in ("--c", "--cont_port"):
        cont_port = a
    elif o in ("--r", "--repeat"):
        repeat = a
    elif o in ("--h", "--help"):
        print("usage: --binary <pktgen_binary> --interface <eth interface> --addr_list" +
              "<addr_list_file> --xdp_port <xdp_p> --ipt_port <ipt_p> --cont_port <cont_p>" +
              "--timeout <timeout> --delay <delay_per_packet> --repeat <repeat_count>")
        sys.exit(1)

f = open(addr_list, "r")
addrs = f.readlines()

for addr in addrs:
    os.system(binary + " --server" + " --interface " + interface + " --addr " + addr +
              " --xdp_port " + xdp_port + " --ipt_port " + ipt_port + " --cont_port " + 
              cont_port + " --delay " + delay + " --timeout " + timeout + " --repeat " + repeat)

